﻿//This is a very messy module... needs love
angular.module('CIMModule', []).config(function ($mdIconProvider) {
    $mdIconProvider
        .defaultFontSet('FontAwesome')
        .fontSet('fas', 'FontAwesome');
}).controller("CIMController", function ($scope, $http, $filter) { 
    //Various default settings
    $scope.Epics = null;
    $scope.ShowEpics = false;
    $scope.ShowSprints = false;
    $scope.taskTotal = 0;
    $scope.start = 0;
    $scope.allTasks = [];
    $scope.showInfo = false;
    $scope.CIMError = false;
    selected = null;
    previous = null;
    $scope.selectedIndex = 0;
    $scope.FTE = 6;
    $scope.PTO = 0;
    $scope.sprintEndDate = new Date().getTime();
    $scope.workDaysLeftStore = 0;
    $scope.PTOMax = 0;
    $scope.refreshStatus = false;
    $scope.refresh = function(project, taskKey, employee, sprint) {
        start();
        $scope.refreshStatus = true;
        $scope.loadProject(project, sprint);         
    };

    start = function() {
        //Get all the projects - this loads at start
        $http.get("/Jira/GetAllProjects").then(function(response) {
            if (response.data === "CIM MIA") {
                $scope.CIMError = true;
            } else {
                try {
                    $scope.Projects = angular.fromJson(response.data);
                } catch(err) {
                    $scope.CIMError = true;
                }
            }

        }).finally(function () {
            $scope.$watch('selectedIndex', function(current, old) {
                previous = selected;
                selected = $scope.Projects[current];
            });
            });
        $http.get("/Jira/GetPersonSettings").then(function (response) {
            $scope.ESettings = response.data;
        });        
    };
    start();

    //When a project is selected, query for its tasks and do some math
    $scope.loadProject = function (pn, sprintID = 0) {
        console.log(pn);
        $scope.Tasks = null;
        $scope.assignees = null;
        $scope.Sprints = null;
        $scope.Epics = null;
        $scope.form.$setPristine();
        $scope.PTO = 0;

        //get all the epics under the selected project
        ProjectName = encodeURI(pn);
        $http.get("/Jira/GetProjectEpics/" + ProjectName).then(function (response) {
            $scope.Epics = response.data.issues;
        }).finally(function () {
        });

        //get all the sprints under the selected project
        $http.get("/Jira/GetProjectSprints/" + ProjectName).then(function (response) {
            $scope.Sprints = response.data;
        }).finally(function () {
            //console.log($scope.Sprints);
        });

        //function to get how many M-F days are inbetween two dates. Uses the moment.js library
        function getBusinessDays(startDate, endDate) {
            var startDateMoment = moment(startDate);
            var endDateMoment = moment(endDate);
            var days = Math.round(startDateMoment.diff(endDateMoment, 'days') - startDateMoment.diff(endDateMoment, 'days') / 7 * 2);
            if (endDateMoment.day() === 6) {
                days--;
            }
            if (startDateMoment.day() === 7) {
                days--;
            }
            return days;
        }

        //if you chose all, reset the query to all projects
        if (sprintID === 0 || sprintID === "All") {
            url = "/Jira/GetProjectTasks/" + ProjectName + "?start=0";
            $scope.workDaysLeft = 0;
            $scope.EndDate = null;
            $scope.daysLeft = null;
        } else {
            //URL for the specified project and sprint tasks
            url = "/Jira/GetProjectTasks/" + ProjectName + "?start=0&sprintID=" + sprintID.id;

            //Jira stores the date in a non-standard format. Break it down to make a date object.
            var YYYY = sprintID.end.substring(4, 8);
            var DD = sprintID.end.substring(0, 2);
            var MM = sprintID.end.substring(2, 4);
            EndDate = YYYY + "-" + MM + "-" + DD;
            $scope.EndDate = new Date(EndDate);
            var now = new Date().getTime();
            //calculate some info
            $scope.daysLeft = ($scope.EndDate - now) / 1000 / 60 / 60 / 24;
            $scope.workDaysLeft = getBusinessDays($scope.EndDate, now);
            $scope.workDaysLeftStore = $scope.workDaysLeft;
            $scope.PTOMax = $scope.workDaysLeft - 1;
        }

        //get the tasks
        $http.get(url).then(function (response) {
            $scope.Tasks = response.data;
        }).finally(function () {
            //$scope.Tasks = $scope.Tasks.filter(function (obj) {
            //    return obj.fields.status.name !== 'Done';
            //});
            var people = [];
            //for each task, build custom array of people assigned to each task
            angular.forEach($scope.Tasks, function (item) {
                if (item.fields.assignee !== null) {
                    hr = item.fields.timeoriginalestimate - item.fields.timespent;
                    if (item.fields.status.name === 'Done') {
                        hr = 0;
                    }
                    people.push({ uid: item.fields.assignee.accountId, display: item.fields.assignee.displayName, hoursEstimated: item.fields.timeoriginalestimate, hoursSpent: item.fields.timespent, hr: hr, status: item.fields.status.name });
                    var index = $scope.ESettings.findIndex(x => x.accountId === item.fields.assignee.accountId);
                    if (index === -1) {
                        $scope.ESettings.push({
                            "accountId": item.fields.assignee.accountId,
                            "displayName": item.fields.assignee.displayName,
                            "FTE": 6
                        });
                    }                    
                }      
            });
            //get rid of duplicates. We just need a list of unique people in this filter
            var assignees = people.reduce(function (obj, item) {
                obj[item.uid] = obj[item.uid] || [];
                obj[item.uid].push({ display: item.display, hoursEstimated: item.hoursEstimated, hoursSpent: item.hoursSpent, uid: item.uid, hr: item.hr });
                return obj;
            }, {});

            //make it a scope var
            $scope.assignees = assignees;

            $scope.the = 0;
            $scope.ths = 0;
            $scope.thr = 0;
            //for each person, math up all their workloads
            angular.forEach($scope.assignees, function (item) {
                var hoursSpent = 0;
                var hoursE = 0;
                item.forEach(function (task) {
                    hoursSpent = task.hoursSpent + hoursSpent;
                    hoursE = task.hoursEstimated + hoursE;
                    item.display = task.display;
                    item.uid = task.uid;
                });
                item.totalHoursSpent = hoursSpent;
                item.totalHoursEstimated = hoursE;
                item.hr = (item.totalHoursEstimated - item.totalHoursSpent) / 60 / 60;
                if (item.status === 'Done') {
                    item.hr = 0;
                }
                if (item.hr > 0) {
                    $scope.thr = $scope.thr + item.hr;
                }
                $scope.ths = $scope.ths + hoursSpent;
                $scope.the = $scope.the + hoursE;

            });
            $scope.thsminutes = $scope.ths / 60;
            $scope.thshours = Math.floor($scope.thsminutes / 60);
            $scope.thsfte = Math.floor($scope.thshours / $scope.FTE);
            $scope.thsfteremaining = $scope.thshours % $scope.FTE;
            $scope.theminutes = $scope.the / 60;
            $scope.thehours = Math.floor($scope.theminutes / 60);
            $scope.theftehours = Math.floor($scope.thehours / $scope.FTE);
            $scope.thefte = $scope.thehours;
            $scope.thefteremaining = $scope.thehours % $scope.FTE;

            //store all
            $scope.AllTasks = $scope.Tasks;
            $scope.showInfo = true;
            if ($scope.refreshStatus) {
                $scope.filterData($scope.ChosenSprint,$scope.ChosenTask, $scope.ChosenPerson);
            }
            //console.log($scope.Tasks);
        });
        
    };

    //watch for the FTE field to change, if it changes update these vars
    $scope.$watch("FTE", function () {
        $scope.thsfte = Math.floor($scope.thshours / $scope.FTE);
        $scope.thsfteremaining = $scope.thshours % $scope.FTE;
        $scope.thefte = Math.floor($scope.thehours / $scope.FTE);
        $scope.thefteremaining = $scope.thehours % $scope.FTE;
    });

    //watch for the PTO field to change, if it changes update these vars
    $scope.$watch("PTO", function () {
        $scope.workDaysLeft = $scope.workDaysLeftStore;
        $scope.workDaysLeft = $scope.workDaysLeft - $scope.PTO;
    });

    //You chose a filter, run this
    $scope.filterData = function (sprint, taskKey, employee) {
        var people = [];
        $scope.Tasks = $scope.AllTasks;
        if (!taskKey) {
            taskKey = "All";
        }
        if (!employee) {
            employee = "All";
        }
        var keep = true;
        var Skeep = true;
        var Ekeep = true;
        var Tkeep = true;

        //Filter the tasks based on your filter
        //$scope.Tasks = $scope.Tasks.filter(function (obj) {
        //    return obj.fields.status.name !== 'Done';
        //});
        $scope.Tasks = $filter('filter')($scope.Tasks, function (task) {
            if (sprint) {
                Skeep = true;
            }
            //did you filter by employee?
            if (employee) {
                if (employee !== "All") {
                    if (task.fields.assignee !== null) {
                        if (task.fields.assignee.accountId === employee.uid) {
                            Ekeep = true;
                        } else {
                            Ekeep = false;
                        }
                    } else {
                        Ekeep = false;
                    }
                } else {
                    Ekeep = true;
                }
            }
            //filter by task?
            if (taskKey) {
                if (taskKey !== "All") {
                    if (task.key === taskKey) {
                        Tkeep = true;
                    } else {
                        Tkeep = false;
                    }
                } else {
                    Tkeep = true;
                }
            }
            if (Tkeep && Skeep && Ekeep) { keep = true; } else { keep = false; }
            return keep;

        });
        //for each, now filtered, task rebuild the people list
        $scope.Tasks.forEach(function (item) {
            if (item.fields.assignee !== null) {
                hr = item.fields.timeoriginalestimate - item.fields.timespent;
                if (item.fields.status.name === 'Done') {
                    hr = 0;
                }
                people.push({ uid: item.fields.assignee.accountId, display: item.fields.assignee.displayName, hoursEstimated: item.fields.timeestimate, hoursSpent: item.fields.timespent, hr: hr, status: item.fields.status.name });
            }
        });
        var newassignees = people.reduce(function (obj, item) {
            obj[item.uid] = obj[item.uid] || [];
            obj[item.uid].push({ display: item.display, hoursEstimated: item.hoursEstimated, hoursSpent: item.hoursSpent, uid: item.uid, hr: item.hr });
            return obj;
        }, {});
        $scope.assignees = newassignees;
        //sadly do the math again... maybe move to a function
        $scope.the = 0;
        $scope.ths = 0;
        $scope.thr = 0;
        //console.log($scope.Tasks);
        angular.forEach(newassignees, function (item) {
            var hoursSpent = 0;
            var hoursE = 0;
            item.forEach(function (task) {
                hoursSpent = task.hoursSpent + hoursSpent;
                hoursE = task.hoursEstimated + hoursE;
                item.display = task.display;
                item.uid = task.uid;
            });
            item.totalHoursSpent = hoursSpent;
            item.totalHoursEstimated = hoursE;
            $scope.ths = $scope.ths + hoursSpent;
            $scope.the = $scope.the + hoursE;
            item.hr = (item.totalHoursEstimated - item.totalHoursSpent) / 60 / 60;
            if (item.status === 'Done') {
                item.hr = 0;
            }
            $scope.thr = $scope.thr + item.hr;
        });

        $scope.thsminutes = $scope.ths / 60;
        $scope.thshours = Math.floor($scope.thsminutes / 60);
        $scope.thsfte = Math.floor($scope.thshours / 6);
        $scope.thsfteremaining = $scope.thshours % 6;
        $scope.theminutes = $scope.the / 60;
        $scope.thehours = Math.floor($scope.theminutes / 60);
        $scope.theftehours = Math.floor($scope.thehours / 6);
        $scope.thefte = $scope.thehours;
        $scope.thefteremaining = $scope.thehours % 6;
    };

    $scope.removePerson = function (person) {
        var index = $scope.ESettings.indexOf(person);
        $scope.ESettings.splice(index, 1);     
    };
    $scope.getFTE = function (uid) {
        var index = $scope.ESettings.findIndex(x => x.accountId === uid);
        if (index !== -1) {
            return $scope.ESettings[index].FTE;            
        } else {
            return $scope.FTE;
        }
    };

    $scope.saveSettings = function () {
        var jsonObj = angular.toJson($scope.ESettings);

        var url = "/Jira/SavePersonSettings";
        $http.post(url, jsonObj, {
            headers: {
                'Content-type': 'application/json'
            }
        }).then(function (response) {
            if (response.status === 200) {
                console.log("GoodToGo");
            } else {
                console.log("Something went wrong.");
            }
        });
    };
    $scope.loadEpicTasks = function (epicKey, index) {
        var url = "/Jira/GetTasksByEpic/" + epicKey;
        if ($scope.ChosenSprint) {
            url = "/Jira/GetTasksByEpicAndSprint?epicID=" + epicKey + "&sprintID=" + $scope.ChosenSprint.id;
        }
        $http.get(url).then(function (response) {
            $scope.epicTasks = response.data.issues;
        }).finally(function () {
            var keys = [];
            var he = 0;
            var hs = 0;
            $scope.epicTasks.forEach(function (task) {
                keys.push(task.key);
                he = he + task.fields.timeestimate;
                hs = hs + task.fields.timespent;
            });
            var keyString = "";
            keys.forEach(function (key) {
                keyString = keyString + key + ", ";
            });
            keyString = "Included Tasks: " + keyString.slice(0, -2);


            subTasks = {
                key: keyString,
                fields: {
                    timeestimate: he,
                    timespent: hs
                }
            };
            $scope.Epics.splice(index + 1, 0, subTasks);
        });
    };
});