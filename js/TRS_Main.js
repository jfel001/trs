﻿var TRS = angular.module('TRS', [
    'ngRoute',
    'ngMaterial',
    'ngMessages',
    'ngAnimate',
    'ngCookies',
    'CIMModule'
]);

//This function loads the loading bar by setting a variable true/false whenever a promise is waiting.
TRS.run(function ($rootScope) {
    //function to turn the loading bar on
    $rootScope.$on('loading:progress', function () {
        $rootScope.dataLoading = true;
    });
    //function to turn the loading bar off
    $rootScope.$on('loading:finish', function () {
        $rootScope.dataLoading = false;
    });
});

TRS.filter('negativeToZero', function () {
    return function (number) {
        if (isNaN(number)) {
            return number;
        }
        else {
            if (number < 0) {
                return 0.00;
            } else {
                return number;
            }
        }

    };
});


//Factory for the loading bar
TRS.factory('httpInterceptor', ['$q', '$rootScope',
    function ($q, $rootScope) {
        var loadingCount = 0;

        return {
            request: function (config) {
                if (++loadingCount === 1) $rootScope.$broadcast('loading:progress');
                return config || $q.when(config);
            },

            response: function (response) {
                if (--loadingCount === 0) $rootScope.$broadcast('loading:finish');
                return response || $q.when(response);
            },

            responseError: function (response) {
                if (--loadingCount === 0) $rootScope.$broadcast('loading:finish');
                return $q.reject(response);
            }
        };
    }
]).config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('httpInterceptor');
}]);