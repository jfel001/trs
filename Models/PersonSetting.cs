﻿namespace TRS.Standard.Models
{
    public class PersonSetting
    {
            public string accountId { get; set; }
            public string displayName { get; set; }
            public int FTE { get; set; }       

    }
}