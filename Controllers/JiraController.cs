﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Script.Serialization;
using TRS.Standard.Models;
using System.Security.Claims;

namespace TRS.Standard.Controllers
{
    public class JiraController : Controller
    {
        [Authorize]
        public ActionResult TRSHome()
        {
            return View();
        }

        [Authorize]
        public string GetPersonSettings()
        {
            List<PersonSetting> EmployeeSettings = new List<PersonSetting>();
            string path = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Persons.json"); 
            if (System.IO.File.Exists(path))
            {
                try
                {
                     string rawJson = System.IO.File.ReadAllText(path);
                     EmployeeSettings = JsonConvert.DeserializeObject<List<PersonSetting>>(rawJson);
                }
                catch (Exception e)
                {
                }

            }
            var json = new JavaScriptSerializer().Serialize(EmployeeSettings);
            return json;
        }

        [HttpPost]
        [Authorize]
        public void SavePersonSettings()
        {
            Request.InputStream.Seek(0, SeekOrigin.Begin);
            string jsonData = new StreamReader(Request.InputStream).ReadToEnd();
            var cleanJson = JToken.Parse(jsonData).ToString();
            string path = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Persons.json");

            System.IO.File.WriteAllText(path, cleanJson);
        }


        private Token GetToken()
        {
            //Get Authention Token from the CIM
            var client = new HttpClient();
            //Move to encrypted config file
            var uri = "https://localhost:5003/Token/CreateToken";

            //Move to encrypted config file
            HttpContent content = new StringContent("{\"Username\":\"CIMUser\", \"Password\":\"Josh1sS@lty!\"}");
            string result = "";
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            try
            {
                ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                var response = client.PostAsync(uri, content).Result;
                response.EnsureSuccessStatusCode();
                result = response.Content.ReadAsStringAsync().Result;
            }
            catch (HttpRequestException ex)
            {
                return null;
            }
            catch (Exception ex)
            {
                //do something someday
                return null;
            }

            //Create new Token object from response
            Token Token = new Token();
            Token = JsonConvert.DeserializeObject<Token>(result);
            System.Web.HttpContext.Current.Session["CIMToken"] = result;
            return Token;

        }

        private string GetCIM(string url)
        {
            //GET Requests for the CIM
            Token Token = new Token();

            //If you have a token already, use it, otherwise get a new token. 
            //Need to add experation to match token experation
            if (System.Web.HttpContext.Current.Session["CIMToken"] != null)
            {
                string tokenString = System.Web.HttpContext.Current.Session["CIMToken"] as string;
                Token = new Token();
                Token = JsonConvert.DeserializeObject<Token>(tokenString);

            }
            else
            {
                Token = GetToken();
            }

            HttpClient client = new HttpClient();
            try
            {
                ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token.token);
            }
            catch (Exception e)
            {
                return "CIM MIA" + e;
            }
            //Accept self-signed certs - remove in production with valid certs
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
            try
            {
                var response = client.GetAsync(url).Result;
                string result = string.Empty;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsStringAsync().Result;
                }
                return result;
            }
            catch (Exception e)
            {
                return "CIM MIA" + e;
            }
        }

        //JIRA GET Methods
        //need to move url to config
        //add additional validation
        [Authorize]
        public string GetAllProjects()
        {
            var AllProjects = GetCIM("https://localhost:5003/Jira/GetAllProjects");
            return AllProjects;
        }

        [Authorize]
        public string GetProjectEpics(string id)
        {
            string Url = "https://localhost:5003/Jira/GetEpicsByProjectNameAsync?name=" + HttpUtility.UrlEncode(id);
            return GetCIM(Url);
        }

        [Authorize]
        public string GetProjectSprints(string id)
        {
            string Url = "https://localhost:5003/Jira/GetSprintsByProjectNameAsync?name=" + HttpUtility.UrlEncode(id);
            return GetCIM(Url);
        }

        [Authorize]
        public string GetProjectTasks(string id, string start, string sprintID)
        {
            string Url = null;
            if (sprintID == null || sprintID == "")
            {
                 Url = "https://localhost:5003/Jira/GetTasksByProjectName?name=" + HttpUtility.UrlEncode(id) + "&start=" + HttpUtility.UrlEncode(start);
            }
            else
            {
                 Url = "https://localhost:5003/Jira/GetTasksByProjectName?name=" + HttpUtility.UrlEncode(id) + "&start=" + HttpUtility.UrlEncode(start) + "&sprintID=" + HttpUtility.UrlEncode(sprintID);
            }
            return GetCIM(Url);
        }

        [Authorize]
        public string GetTasksByEpic(string id)
        {
            string Url = "https://localhost:5003/Jira/GetTasksByEpic?epicKey=" + HttpUtility.UrlEncode(id);
            return GetCIM(Url);
        }

        [Authorize]
        public string GetTasksByEpicandSprint(string epicID, string sprintID)
        {
            string Url = "https://localhost:5003/Jira/GetTasksByEpicAndSprint?epicKey=" + epicID + "&sprintID=" + sprintID;
            return GetCIM(Url);
        }

    }
}